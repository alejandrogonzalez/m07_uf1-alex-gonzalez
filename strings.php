<?php

echo 'Strings PHP <br/>';

$frase = 'Hola Mundo!';
echo $frase[0];

echo "longitud cadena". strlen($frase) .'<br/>';
echo "número palabras ". str_word_count($frase) . '<br/>';
echo "Inverso " . strrev($frase) . '<br/>';
echo "Buscar posicion " . strpos($frase, "Mundo") . '<br/>';
echo "Reemplazar " . str_replace("Mundo","Planeta",$frase) . '<br/>';
echo "Reemplazar con regexp" . preg_replace("/W[a-z] + /u","Nope",$frase). '<br/>';

?>
